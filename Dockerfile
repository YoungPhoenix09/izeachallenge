FROM node

WORKDIR /usr/src/tinyUrlApp

COPY package.json ./

RUN npm install

COPY . .

EXPOSE 3000
CMD ["npm", "start"]
