const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json', {
  defaultValue: {urls: [], currentId: 0}
})
const db = low(adapter)

const randStr = require('randomstring');

//Read contents of file
//console.log('Reading db file')
//db.read()

let getUrl = (url) => {
  console.log(`Looking up URL: ${url}`)
  let urlObj = db.get('urls').find({url: url}).value()

  console.log(urlObj)
  return urlObj || null
}

let addUrl = (url) => {
  let urlObj = getUrl(url)

  if (urlObj === null) {
    //Generate short link that isn't in use
    let shortLink = ""
    do {
      shortLink = randStr.generate({
        length: 5,
        charset: 'alphabetic'
      })
    } while (getUrlByTinyUrl(shortLink) !== null)

    console.log(`Chosen short link is ${shortLink}`)

    //Get current ID #
    let id = db.get('currentId').value()

    //Add url
    db.get('urls').push({id: id, tinyUrl: shortLink, targetUrl: url}).write()

    //Increment current ID #
    db.update('currentId', n => n + 1).write()

    //
    return db.get('urls').find({id: id}).value()
  } else {
    return urlObj
  }
}

let getUrlByTinyUrl = (tinyUrl) => {
  console.log(`Looking up tiny URL: ${tinyUrl}`)
  let urlObj = db.get('urls').find({tinyUrl: tinyUrl}).value()

  console.log(urlObj)
  return urlObj || null
}

let getUrlById = (id) => {
  console.log(`Looking up ID: ${id}`)
  let urlObj = db.get('urls').find({id: parseInt(id)}).value()

  console.log(urlObj)
  return urlObj || null
}

let removeUrl = (urlId) => {
  db.get('urls').remove({id: urlId})

  console.log(`Removed entry for URL ID ${urlId}`)
}

dbFuncs = {
  getUrl,
  addUrl,
  getUrlByTinyUrl,
  getUrlById,
  removeUrl
}

module.exports = dbFuncs
