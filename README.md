# README #

Preface: The approach I am normally accustomed to for quickly getting an app together is using MeteorJS as it enables access to a DB and routing framework with very little effort. However, I believe that to be overkill and instead took the time to re-familiarize myself with ExpressJS (it's been a little bit).  I also had never used Docker before so I did a little ramping up with that as well.

### What this service does ###

* It allows you to submit urls and receive a shortened URL reference back
* Allows you to find URL info via the shortened URL or its ID

### Setup ###

This is actually pretty simple with the whole repository cloned.

You can use the following to build and run
```sh
docker build -t tiny_url_app

docker run -p 3000:3000 tiny_url_app
```

Or you can load the docker archive
```sh
docker load --input tinyUrl.tar

docker run -p 3000:3000 tiny_url_app
```

### Testing ###

* Test using your favorite REST client (I used the Advanced REST Client from Chrome)
* To get a shortened URL, submit a POST request to http://localhost:3000/addUrl
  * Body format should be JSON object like the following:
  ```js
  {url: "http:/my/url/here"}
  ```
* After a URL has been submitted for shortening, you can use this service to link to the URL via its shortened form
  * i.e. http://localhost:3000/wEtlg
* To get back information about a shortened URL, you have two options:
  * Send a GET request using the shortened URL to http://localhost:3000/find/
    * i.e. http://localhost:3000/find/wEtlg
  * Send a GET request using the ID of the shortened URL to http://localhost:3000/find/id
    * i.e. http://localhost:3000/find/id/3
