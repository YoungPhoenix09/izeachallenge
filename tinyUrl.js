const persist = require('./persist');

const express = require('express');
const tinyUrlApp = express();
const router = express.Router();

//Set port here
const PORT = 3000;

router.get('/', (req, res) => {
  //Show home
  res.send(`<h1>Welcome to the Tiny URL Service!</h1>
    <ul>
    <li>Send a POST request of the URL you want to shorten to '/addUrl'.</li>
    <li>You can redirect by using http://localhost:3000/shortLinkHere.</li>
    <li>Send a GET request to '/find' using the shortened URL to get it's information.</li>
    <li>You can also receive the same information by sending a GET request to '/find/id' using the shortened URL's ID.</li>
    </ul>`);
});

router.get('/:tinyUrl', (req, res) => {
  let url = ""

  if (!!req.params.tinyUrl) {
    //Lookup target URL
    let urlObj = persist.getUrlByTinyUrl(req.params.tinyUrl)
    url = (!!urlObj) ? urlObj.targetUrl : ""
  }

  if (!!url) {
    //Redirect to URL
    res.redirect(url)
  }
  else {
    //Show home
    res.send(`<h1>Welcome to the Tiny URL Service!</h1>
      <ul>
      <li>Send a POST request of the URL you want to shorten to '/addUrl'.</li>
      <li>You can redirect by using http://localhost:3000/shortLinkHere.</li>
      <li>Send a GET request to '/find' using the shortened URL to get it's information.</li>
      <li>You can also receive the same information by sending a GET request to '/find/id' using the shortened URL's ID.</li>
      </ul>`);
  }
});

router.get('/find/:tinyUrl', (req, res) => {
  let urlObj = ""

  if (!!req.params.tinyUrl) {
    //Lookup target URL
    urlObj = persist.getUrlByTinyUrl(req.params.tinyUrl)
  }

  if (!!urlObj) {
    //Redirect to URL
    res.json(urlObj)
  }
  else {
    //Show home
    res.status(404).send(`No URL info found!`);
  }
});

router.get('/find/id/:id', (req, res) => {
  let urlObj = ""

  if (!!req.params.id) {
    //Lookup target URL
    urlObj = persist.getUrlById(req.params.id)
  }

  if (!!urlObj) {
    //Redirect to URL
    res.json(urlObj)
  }
  else {
    //Show home
    res.status(404).send(`No URL info found!`);
  }
});

//Route for adding new URL link
router.post('/addUrl', (req, res) => {
  if (!!req.body) {
    if (!!req.body.url) {
      let urlObj = persist.addUrl(req.body.url)

      //Send url data
      res.json(urlObj)
    } else {
      res.status(500).send("No 'url' value provided in request body!  Format of object should be {url: 'my/url/here'}.")
    }
  } else {
    res.status(500).send("No body provided!  Body should be JSON in the format of {url: 'my/url/here'}.")
  }
});

tinyUrlApp.use(express.json()); //Add JSON body parser
tinyUrlApp.use(router);

// Start listen on port
tinyUrlApp.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});
